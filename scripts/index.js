// Tabs change color on click
$("#home-tab").click(function() {
    $("#home-page").css({display: "flex"});
    $("#home-tab").addClass("active-tab");
    $("#recent-page").css({display: "none"});
    $("#recent-tab").removeClass("active-tab");
});

$("#recent-tab").click(function() {
    $("#recent-page").css({display: "flex"});
    $("#recent-tab").addClass("active-tab");
    $("#home-page").css({display: "none"});
    $("#home-tab").removeClass("active-tab");
});

// Slide Menu Open/Close
$("#slide-menu-button").click(function() {
    $("#slide-menu").toggleClass("slide-menu-show");
});

$("#categories-dropdown").click(function closeCategories() {
    $("#slide-menu-categories-wrap").toggleClass("slide-menu-hidden");
    $("#category-arrow").toggleClass("fa-angle-down");
});

// Show image modal
$("#close-modal").click(() => {
    $("#modal, #modal-inner, #inner-image-container").css({"display": "none"})
});

$(".img").click(function openImage() {
    $("#modal-image").attr("src", document.getElementById(event.target.id).src);
    $("#modal, #modal-inner, #inner-image-container").css({"display": "flex"});
    $("#image-size").text(document.getElementById(event.target.id).naturalWidth + 
    " x " + document.getElementById(event.target.id).naturalHeight);
});

// Upload Image
let chooseCategory = document.getElementById("choose-category-p");
let categoryDropmenu = document.getElementById("upload-categories-drop-menu")

let chooseTags = document.getElementById("choose-tags-p");
let tagsDropmenu = document.getElementById("upload-tags-drop-menu")
let chooseImage = document.getElementById("choose-image");

$("#upload-button").click(function() {
    $("#upload-outer-modal").css({display: "flex"});
});

// Categories
$("#close-upload-modal").click(function() {
    $("#upload-outer-modal").css({display: "none"});
    chooseCategory.innerHTML = "Choose Category";
    chooseImage.value = "";
});

$("#upload-choose-category").click(function() {
    document.getElementById("upload-category-down").classList.toggle("fa-angle-up");
    categoryDropmenu.classList.toggle("upload-categories-drop-menu-display");
});

$(".upload-category-item").click(function() {
    let innerText = document.getElementById(event.target.id).innerHTML;
    chooseCategory.innerHTML = innerText;
    categoryDropmenu.classList.toggle("upload-categories-drop-menu-display");
});

// Tags
$("#upload-choose-tags").click(function() {
    tagsDropmenu.classList.toggle("upload-categories-drop-menu-display");
});

$(".upload-tags-item").click(function() {
    chooseTags.innerHTML = document.getElementById(event.target.id).innerHTML;
    tagsDropmenu.classList.toggle("upload-categories-drop-menu-display");
});