// Array med alla bilder.
let images = [];

let addImages = document.getElementById("recent-images-container").getElementsByTagName("img");

for (let img = 0; img < addImages.length; img++) {
    images.push(addImages[img]);
}

// OLD CODE
// Letar rätt på vilka bilder som matchar sökordet
/*
$("#search-tags-input").on("keypress", function(e) {
    if (e.which == 13 && this.value.length > 0) {          
        images.forEach(img => {
            if(img.classList.contains(this.value)) {
                document.getElementById("images-wrap").appendChild(img);    
            } 
        });
    }
});
*/
// OLD WAY
/*
$("#search-button").on("click", () => {
    images.forEach(img => {
        if(img.tag == document.getElementById("search-tags-input").value) {
            if($("#images-container > .img").hasClass(img.tag)) {
                console.log("test");
            }               
        }          
    });
});*/

// on keypress
$("#search-tags-input").on("keypress", function(e) {
    if (e.which == 13 && this.value.length > 0) {
        document.getElementById("images-container").innerHTML = "";  
        document.getElementById("search-result-info").textContent = `"${this.value}"`;        
        let result = images.filter(img => img.classList.contains(this.value));
        result.forEach(img => {
            document.getElementById("images-container").appendChild(img);
        });
    }
});

let searchValue = document.getElementById("search-tags-input");

// on search click
$("#search-button").on("click", function() {
    if (searchValue.value.length > 0) {
        document.getElementById("images-container").innerHTML = "";  
        document.getElementById("search-result-info").textContent = `"${searchValue.value}"`;        
        let result = images.filter(img => img.classList.contains(searchValue.value));
        result.forEach(img => {
            document.getElementById("images-container").appendChild(img);
        });
    }
});